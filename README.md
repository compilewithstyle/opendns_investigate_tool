# README #

Helper script for the security office to have easier access to the OpenDNS investigate tool -- eliminates having to remember curl syntax and investigate file paths.

Don't forget to set the 'OPENDNS_API_TOKEN' in your bash environment to your OWN api key.